#include <osg/Group>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>

int main(){
//root node
osg::ref_ptr<osg::Group> rootnode = new osg::Group;

//three chlidren nodes
osg::ref_ptr<osg::Node> model1 = osgDB::readNodeFile("dumptruck.osg");
osg::ref_ptr<osg::Node> model2 = osgDB::readNodeFile("cow.osg");
osg::ref_ptr<osg::Node> model3 = osgDB::readNodeFile("cessna.osg");

rootNode->addChild(model1.get());
rootNode->addChild(model2.get());
rootNode=>addChild(model3.get());

osgViewer::Viewer viewer;
viewer.setScreenData( rootNode.get());
viewer.setUpViewInWindow(50,50,800,600);
viewer.run();

}
